<?php 
/**
* Description: Lionlab variants field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

?>

<section class="variants">

	<div class="wrap hpad variants__container">
		<div class="row flex flex--wrap flex--center">
			<?php  
				$title = get_sub_field('title');
				$img = get_sub_field('img');
				$meter = get_sub_field('meter');
				$text = get_sub_field('text');
				$link = get_sub_field('link'); 
			?>

			<div class="col-sm-4 variants__product-img wow bounceIn">
				<?php if ($img) : ?>
				<img src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<?php endif; ?>
			</div>

		
			<div class="col-sm-4 variants__item wow bounceIn">
			
				<h2 class="variants__title"><?php echo esc_html($title); ?></h2>
				<p><?php echo esc_html($text); ?></p>

				<div class="variants__wrap flex flex--center">
					<?php if ($link) : ?>
					<a class="btn btn--yellow" target="_blank" href="<?php echo esc_url($link); ?>"><span>Køb</span></a> 
					<?php endif; ?>
					<div class="fb-share-button variants__fb" data-href="<?php echo the_permalink(); ?>" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>" class="fb-xfbml-parse-ignore">Del</a></div>
				</div>
			</div>

			<div class="col-sm-4 variants__product-featured variants__product-featured--<?php echo esc_attr($bg); ?> wow bounceIn">
				<?php if ($meter) : ?>
				<img src="<?php echo esc_url($meter['url']); ?>" alt="<?php echo esc_attr($meter['alt']); ?>">
				<?php endif; ?>
			</div>
			
		</div>
	</div>

</section>